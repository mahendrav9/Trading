create table Order_Transaction 
(ID int NOT NULL,
Ask_rate Float(8,4),
Bid_rate Float(8,4),
Quantity Float(8,4),
 FOREIGN KEY (Id) REFERENCES Order_Table(Id));