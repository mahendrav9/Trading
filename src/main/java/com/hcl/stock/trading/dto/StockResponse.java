package com.hcl.stock.trading.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StockResponse {

	@JsonProperty(value="StatusCode")
	public Integer statusCode;
	
	@JsonProperty(value="StatusMsg")
	public String statusMsg;
	
	@JsonProperty(value="Stock")
	public List<Stock> stock;

	/**
	 * @return the statusCode
	 */
	public Integer getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the statusMsg
	 */
	public String getStatusMsg() {
		return statusMsg;
	}

	/**
	 * @param statusMsg the statusMsg to set
	 */
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	/**
	 * @return the stock
	 */
	public List<Stock> getStock() {
		return stock;
	}

	/**
	 * @param stock the stock to set
	 */
	public void setStock(List<Stock> stock) {
		this.stock = stock;
	}
	
	
	
}
