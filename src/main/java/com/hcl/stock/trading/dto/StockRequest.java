package com.hcl.stock.trading.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StockRequest {

	@JsonProperty(value="UserId")
	public String userId;
	
	@JsonProperty(value="StockType")
	public String type;
	
	@JsonProperty(value="Quantity")
	public Integer quantity;
	
	@JsonProperty(value="Symbol")
	public String symbol;

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the symbol
	 */
	public String getSymbol() {
		return symbol;
	}

	/**
	 * @param symbol the symbol to set
	 */
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	
	
}
