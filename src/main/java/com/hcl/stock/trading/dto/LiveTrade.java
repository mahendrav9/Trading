package com.hcl.stock.trading.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LiveTrade {

	@JsonProperty(value="Id")
	public Integer id;
	
	@JsonProperty(value="Symbol")
	public String symbol;
	
	@JsonProperty(value="Quantity")
	public Integer quantity;
	
	@JsonProperty(value="BidPrice")
	public Integer bidPrice;
	
	@JsonProperty(value="AskPrice")
	public Integer askPrice;
	
	@JsonProperty(value="Time")
	public Integer time;

	/**
	 * 
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the symbol
	 */
	public String getSymbol() {
		return symbol;
	}

	/**
	 * @param symbol the symbol to set
	 */
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the bidPrice
	 */
	public Integer getBidPrice() {
		return bidPrice;
	}

	/**
	 * @param bidPrice the bidPrice to set
	 */
	public void setBidPrice(Integer bidPrice) {
		this.bidPrice = bidPrice;
	}

	/**
	 * @return the askPrice
	 */
	public Integer getAskPrice() {
		return askPrice;
	}

	/**
	 * @param askPrice the askPrice to set
	 */
	public void setAskPrice(Integer askPrice) {
		this.askPrice = askPrice;
	}

	/**
	 * @return the time
	 */
	public Integer getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(Integer time) {
		this.time = time;
	}
	
	
}
