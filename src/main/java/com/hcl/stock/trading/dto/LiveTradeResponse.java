package com.hcl.stock.trading.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LiveTradeResponse {

	@JsonProperty(value="StatusCode")
	public Integer statusCode;
	
	@JsonProperty(value="StatusMsg")
	public String statusMsg;
	
	@JsonProperty(value="Data")
	public List<LiveTrade> data;

	/**
	 * @return the statusCode
	 */
	public Integer getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the statusMsg
	 */
	public String getStatusMsg() {
		return statusMsg;
	}

	/**
	 * @param statusMsg the statusMsg to set
	 */
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	/**
	 * @return the data
	 */
	public List<LiveTrade> getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(List<LiveTrade> data) {
		this.data = data;
	}
	
	
}
