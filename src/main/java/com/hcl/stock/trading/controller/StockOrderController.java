package com.hcl.stock.trading.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.stock.trading.dto.LiveTradeResponse;
import com.hcl.stock.trading.dto.StockRequest;
import com.hcl.stock.trading.dto.StockResponse;
import com.hcl.stock.trading.service.StockService;

@RestController
public class StockOrderController {
	
	@Autowired
	public StockService service;
	
	/**
	 * This method will be called when new request comes.
	 * @return
	 */
	@RequestMapping(value="/create",method=RequestMethod.POST)
	public ResponseEntity<StockResponse> publishRequest(@ModelAttribute(value="stockRequest") StockRequest request){
		return new ResponseEntity<StockResponse>(new StockResponse(),HttpStatus.OK);
	}
	
	
	/**
	 * This method will be called for live data.
	 * @return
	 */
	@RequestMapping(value="/retrieve",method=RequestMethod.GET)
	public ResponseEntity<LiveTradeResponse> retrieveData(){
		return new ResponseEntity<LiveTradeResponse>(new LiveTradeResponse(),HttpStatus.OK);
	}
	
	
	/**
	 * This method will be called for trading graph for specific Id.
	 * @return
	 */
	@RequestMapping(value="/retrieve/{Id}",method=RequestMethod.GET)
	public ResponseEntity<LiveTradeResponse> retrieveData(@PathVariable(value="Id") Integer id){
		return new ResponseEntity<LiveTradeResponse>(new LiveTradeResponse(),HttpStatus.OK);
	}

}
