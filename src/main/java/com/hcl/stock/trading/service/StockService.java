package com.hcl.stock.trading.service;

import java.util.List;

import com.hcl.stock.trading.dto.LiveTrade;
import com.hcl.stock.trading.dto.Stock;
import com.hcl.stock.trading.dto.StockRequest;

public interface StockService {
	
	public List<Stock> publish(StockRequest request);
	
	public List<LiveTrade> retrieveData();
	
	public List<LiveTrade> retrieveDataBasedOnId(Integer id);

}
